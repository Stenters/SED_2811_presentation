/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas
 * Date:       1/24/2019
 */
package presentation;

/**
 * Interface for types that can be cloned
 * @param <ConcreteType> concrete type to clone
 */
public interface Prototype <ConcreteType extends Prototype> {
    /**
     * Method to clone an object
     * @return the new object
     */
    ConcreteType clone();
}

/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas
 * Date:       1/24/2019
 */
package presentation;

import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import presentation.game.CollisionBox;
import presentation.game.FileUtility;
import presentation.game.GameEntity;
import presentation.game.HUDText;
import presentation.game.HealthBar;
import presentation.game.Input;
import presentation.game.Sprite;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Class for the player object in the game, is responsible for manipulating the ship entity
 * controlled by the player.
 */
public class Player implements GameEntity {
    private Sprite sprite;
    private double x = 200;
    private double y = 300;
    private CollisionBox collisionBox;
    private HealthBar healthBar;
    private Bullet prototypeBullet;


    private static final int PARTICLE_OFFSET = 14;
    private static final int BULLET_OFFSET = 3;

    private double forwardSpeed;
    private double reverseSpeed;
    private double xSpeed;
    private double maxHp;
    private double hp;
    private int attackSpeed;

    private int attackCounter;

    private static final int HEALTHBAR_HEIGHT = 10;

    /**
     * Constructor for Player object, initializes sprite things and activates itself as an entity
     */
    public Player(){
        prototypeBullet = new Bullet(new File("file:@../../res/stats/playerBullet"));

        Map<String, String> dataValues =
                FileUtility.loadDataFromFile(new File("file:@../../res/stats/playerStats"));

        forwardSpeed = Double.parseDouble(dataValues.get("ForwardSpeed"));
        reverseSpeed = Double.parseDouble(dataValues.get("ReverseSpeed"));
        xSpeed = Double.parseDouble(dataValues.get("XSpeed"));
        maxHp = Double.parseDouble(dataValues.get("MaxHP"));
        hp = maxHp;
        attackSpeed = Integer.parseInt(dataValues.get("AttackSpeed"));

        Sprite.registerImage("playerShipForward", "file:@../../res/images/player.png");
        Sprite.registerImage("playerShipLeft", "file:@../../res/images/playerLeft.png");
        Sprite.registerImage("playerShipRight", "file:@../../res/images/playerRight.png");

        sprite = new Sprite(this, "playerShipForward", 10, true);
        collisionBox = new CollisionBox("player", this, sprite.getImage().getWidth() * .5);
        healthBar = new HealthBar(0, Controller.CANVAS_WIDTH,
                Controller.CANVAS_HEIGHT - HEALTHBAR_HEIGHT, HEALTHBAR_HEIGHT,
                Paint.valueOf("FF0000"), Paint.valueOf("00FF00"));
        newEntities.add(this);
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    /**
     * Player loop looks for user input and moves and/or shoots based on it
     */
    @Override
    public void loop(){
        boolean leftPressed = Input.getKey(KeyCode.A);
        boolean rightPressed = Input.getKey(KeyCode.D);

        boolean forwardPressed = Input.getKey(KeyCode.W);
        boolean reversePressed = Input.getKey(KeyCode.S);

        double currentXSpeed = 0;

        if ((leftPressed && rightPressed) || !(leftPressed || rightPressed)) {
            sprite.setImageKey("playerShipForward");
        } else if (leftPressed){
            currentXSpeed = -xSpeed;
            sprite.setImageKey("playerShipLeft");
        } else {
            currentXSpeed = xSpeed;
            sprite.setImageKey("playerShipRight");
        }

        x += currentXSpeed * Controller.LOOP_TIME;

        if (forwardPressed && !reversePressed){
            new FireParticle(x, y + PARTICLE_OFFSET, currentXSpeed);
            new FireParticle(x, y + PARTICLE_OFFSET, currentXSpeed);
            new FireParticle(x, y + PARTICLE_OFFSET, currentXSpeed);
            y -= forwardSpeed * Controller.LOOP_TIME;
        }
        if (reversePressed){
            y += reverseSpeed * Controller.LOOP_TIME;
        } else {
            new FireParticle(x, y + PARTICLE_OFFSET, currentXSpeed);
            new FireParticle(x, y + PARTICLE_OFFSET, currentXSpeed);
        }

        boolean spacePressed = Input.getKey(KeyCode.SPACE);
        if(spacePressed && (--attackCounter <= 0)){
            Bullet newBullet = prototypeBullet.clone();
            newBullet.setPosition(x, y - BULLET_OFFSET);
            attackCounter = attackSpeed;
        }

        checkBounds();

        List<GameEntity> collidedBullets = collisionBox.getCollisions("enemyBullets");
        if(collidedBullets != null){
            for(GameEntity entity : collidedBullets){
                if(entity instanceof Bullet){
                    hp -= ((Bullet) entity).getDamage();
                    healthBar.setPercent(hp / maxHp);
                    entity.kill();
                    if(hp <= 0){
                        kill();
                    }
                }
            }
        }
    }

    @Override
    public void kill(){
        deadEntities.add(this);
//        healthBar.remove();
        sprite.remove();
        collisionBox.remove();
        new HUDText("Defeat", Controller.CANVAS_WIDTH * .5,
                Controller.CANVAS_HEIGHT * .5, Paint.valueOf("DD0000"), Font.font(96));
    }

    //helper method to keep the player within the scree bounds
    private void checkBounds(){
        Image image = sprite.getImage();
        if (x - image.getWidth() * .5 < 0){
            x = image.getWidth() * .5;
        }else if (x + image.getWidth() * .5 > Controller.CANVAS_WIDTH){
            x = Controller.CANVAS_WIDTH - image.getWidth() * .5;
        }
        if (y + image.getHeight() * .5 > Controller.CANVAS_HEIGHT - HEALTHBAR_HEIGHT){
            y = Controller.CANVAS_HEIGHT - image.getHeight() * .5 - HEALTHBAR_HEIGHT;
        }else if (y - image.getHeight() * .5 < 0){
            y = image.getHeight() * .5;
        }
    }

}

/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas
 * Date:       1/24/2019
 */
package presentation.game;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Paint;

/**
 * Class for a healthbar element that shows the rough percent of a value
 */
public class HealthBar extends HUDElement{
    private double x;
    private double width;
    private double y;
    private double height;

    private double percent = 1;

    private Paint background;
    private Paint foreground;

    /**
     * Constructor, sets values
     * @param x x position of the left side of the bar
     * @param width width of the bar
     * @param y y position of the top of the bar
     * @param height height of the par
     * @param background background color
     * @param foreground foreground color
     */
    public HealthBar(double x, double width, double y, double height,
                     Paint background, Paint foreground){
        super();
        this.x = x;
        this.width = width;
        this.y = y;
        this.height = height;
        this.background = background;
        this.foreground = foreground;
    }

    public void setXY(double x, double y){
        this.x = x;
        this.y = y;
    }

    public void setPercent(double percent){
        this.percent = percent;
    }

    /**
     * Draws the healthbar
     * @param gc graphics context to render to
     */
    @Override
    public void draw(GraphicsContext gc){
        gc.setFill(background);
        gc.fillRect(x, y, width, height);
        gc.setFill(foreground);
        gc.fillRect(x, y, width * percent, height);
    }
}

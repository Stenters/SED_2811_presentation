/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas
 * Date:       1/24/2019
 */
package presentation.game;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Utility class that has helper methods to deal with loading data from files
 */
public class FileUtility {
    /**
     * Method that will read a file of key value pairs and return a map of their data
     * @param file what file to load from
     * @return a map of data
     */
    public static Map<String, String> loadDataFromFile(File file){
        Map<String, String> values = null;
        try {
            Scanner input = new Scanner(file);

            values = new HashMap<>();

            while (input.hasNextLine()) {
                String line = input.nextLine();
                String[] parts = line.split(": ");
                if(parts.length == 2) {
                    values.put(parts[0], parts[1]);
                } else {
                    System.err.println("Bad input line: " + line);
                }
            }

        } catch (IOException e){
            System.err.println("Unable to read file: " + file.getAbsolutePath());
        }
        return values;
    }
}

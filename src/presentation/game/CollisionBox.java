/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas
 * Date:       1/24/2019
 */
package presentation.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * class for a Collision area that can interact with other collision areas
 */
public class CollisionBox {
    //static list of groups of collision boxes
    private static Map<String, List<CollisionBox>> collisionGroups = new HashMap<>();

    private String collisionGroup;
    private GameEntity owner;
    private double radius;
    private double xOffset = 0;
    private double yOffset = 0;

    /**
     * Constructor for a collision box that initializes fields and puts itself to a group
     * @param collisionGroup what group name the collision box belongs to
     * @param owner what entity owns the box
     * @param radius what radius for the circle
     */
    public CollisionBox(String collisionGroup, GameEntity owner, double radius){
        this.collisionGroup = collisionGroup;
        this.owner = owner;
        this.radius = radius;

        if(!collisionGroups.containsKey(collisionGroup)){
            collisionGroups.put(collisionGroup, new ArrayList<>());
        }
        collisionGroups.get(collisionGroup).add(this);
    }

    public void setOffsets(double xOff, double yOff){
        xOffset = xOff;
        yOffset = yOff;
    }

    /**
     * Method to find all of the other collision boxes that intersect with this one
     * @param collisionGroup what group to check against
     * @return a list of game entities, or null if no collisions
     */
    public List<GameEntity> getCollisions(String collisionGroup){
        List<GameEntity> result = null;
        if(collisionGroups.containsKey(collisionGroup)) {
            List<CollisionBox> group = collisionGroups.get(collisionGroup);
            double x = owner.getX() + xOffset;
            double y = owner.getY() + yOffset;
            for (CollisionBox other : group) {
                if(other != this) {
                    double distance = getDistance(x, y, other.getX(), other.getY());
                    if (distance < radius + other.radius) {
                        if (result == null) {
                            result = new ArrayList<>();
                        }
                        result.add(other.owner);
                    }
                }
            }
        }
        return result;
    }

    /**
     * cleanup method
     */
    public void remove(){
        collisionGroups.get(collisionGroup).remove(this);
    }

    private double getX(){
        return owner.getX() + xOffset;
    }

    private double getY(){
        return owner.getY() + yOffset;
    }

    private static double getDistance(double x1, double y1, double x2, double y2){
        double x = x1 - x2;
        double y = y1 - y2;
        return Math.sqrt(x * x + y * y);
    }
}

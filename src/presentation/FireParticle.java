/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Pattern Presentation
 * Author:     Joshua Spleas
 * Date:       1/24/2019
 */
package presentation;

import presentation.game.GameEntity;
import presentation.game.Sprite;

/**
 * Class that represents a basic fire like particle to be emitted by the player ship
 */
public class FireParticle implements GameEntity {
    Sprite sprite;
    private static final double DECAY_RATE = .02;
    private static final double DRIFT_MAX = 64;
    private static final double VERTICAL_SCALAR= 1.25;
    private static final double BASE_SPEED_SCALAR = .3;
    private double x;
    private double y;
    private double drift;

    /**
     * constructor for the particle, initializes its sprite and adds itself to the entity list
     * @param x initial x
     * @param y initial y
     * @param baseSpeed base horizontal speed of the particle
     */
    public FireParticle(double x, double y, double baseSpeed){
        this.x = x;
        this.y = y;
        drift = (baseSpeed * BASE_SPEED_SCALAR) + (Math.random() * 2 * DRIFT_MAX) - DRIFT_MAX;
        sprite = new Sprite(this, "fireParticle", 1, true);
        Sprite.registerImage("fireParticle", "file:@../../res/images/particle.png");
        newEntities.add(this);
    }

    /**
     * Method to move the bullet down, decay its visibility, and kill itself when it is no longer
     * visible
     */
    @Override
    public void loop(){
        y += Controller.SCROLL_SPEED * Controller.LOOP_TIME * VERTICAL_SCALAR;
        x += drift * Controller.LOOP_TIME;
        sprite.setAlpha(sprite.getAlpha() - DECAY_RATE);
        if(sprite.getAlpha() <= 0.01 ||
                y + sprite.getImage().getHeight() * .5 > Controller.CANVAS_HEIGHT){
            kill();
        }
    }

    /**
     * cleans itself up
     */
    @Override
    public void kill() {
        sprite.remove();
        deadEntities.add(this);
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }
}
